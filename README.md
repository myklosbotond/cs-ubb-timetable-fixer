# CS UBB Timetable fixer

This is a userscript designed to make the timetable page of the Babeș-Bolyai University more readable and to add other functionalities.

## Functionalities

The project currently offers:
* Highlighting table rows according to the type of class
    * Yellow - Course
    * Blue - Seminar
    * Green - Laboratory
* Bringing one's group table to the front
* Automatic highlighting of odd/even week classes based on the current date
* Hiding other halfgroup's classes
* Hiding disciplines
* Renaming disciplines
* Searching for classes based on time and day or discipline and type
* Print view with each table on separate page (Mostly for generating PDF)
* Settings import/export
* **BETA:** Creating a custom timetable by taking classes from any table

## Install

Go and get it from [Greasyfork](https://greasyfork.org/en/scripts/33842-cs-ubb-timetable-fixer)

## Authors

* **Miklós Botond**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
