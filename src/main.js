(function () {
    'use strict';

    loadOptions();
    setupCss();
    setupFunctions();

    structureHTML();
    injectSettingHTML();

    getDisciplinesList();

    setEventHandlers();
    updateSettingsUi();

    runFixer();
})();
