// ==UserScript==
// @name         CS UBB Timetable Fixer
// @namespace    http://tampermonkey.net/
// @version      2.3.6
// @description  A userscript that enhances the tabelar timetables for CS UBB students.
// @author       myklosbotond
// @match        http*://www.cs.ubbcluj.ro/files/orar/*/tabelar/*.html
// @exclude      http*://www.cs.ubbcluj.ro/files/orar/*/tabelar/index.html
// @require      https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js
// @require      https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js
// @resource     bootstrap_CSS https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css
// @resource     jqUI_CSS https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.css
// @grant        GM_addStyle
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_getResourceText
// ==/UserScript==

/*
 * jshint esversion: 6
 * jshint esnext: true
*/
