
function initCustomTable() {
    const grp = $('#custom-group-select').val();
    const halfGrp = parseInt($('#custom-half-group-select').val(), 10);
    const otherHalf = 3 - halfGrp;
    const otherRegEx = new RegExp(`[0-9]+/${otherHalf}`);
    const table = $('.table-group-' + grp);

    table.find('tr:has(td)')
        .filter((_, row) => {
            const $row = $(row);
            const origDiscName = $row.find(`.column-${disciplineColumn}`).data("original");
            const formation = $row.find(`.column-formatia`).text();

            const halfOk = !otherRegEx.test(formation);

            return halfOk && !options.disciplinesToHide.includes(origDiscName)
        })
        .each(function () {
            let rowId = $(this).attr('data-id');
            if (rowId && rowId.length) {
                options.customTable.push(rowId);
            }
        });
}
