//color in just the curs / laboratory column on unselected

function setupCss() {
    'use strict';

    let jqUI_CssSrc = GM_getResourceText("jqUI_CSS");
    jqUI_CssSrc = jqUI_CssSrc.replace(/url\(['"]images\/ui\-bg_.*00\.png['"]\)/g, "");
    jqUI_CssSrc = jqUI_CssSrc.replace(/url\(['"]images\/ui\-icons_.*\.png['"]\)/g, "");

    GM_addStyle(jqUI_CssSrc);

    let bootstrap_Css = GM_getResourceText("bootstrap_CSS");
    GM_addStyle(bootstrap_Css);

    //Insert the styles from the css:
    //<style.css>

}
