function removeElement(array, element) {
    const dataIdx = array.indexOf(element);
    if (dataIdx >= 0) {
        array.splice(dataIdx, 1);
    }
}

function structureHTML() {
    'use strict';

    let html = $('center').html();
    $('center').html('<div id="tables-wrapper"></div>');
    let wrapper = $('#tables-wrapper');
    wrapper.html(html);

    wrapper
        .find('h1:first')
        .insertBefore(wrapper)
        .attr('id', 'title-heading');

    wrapper.find('table').each(function () {
        let columns = [];
        let grp = $(this).prev('h1').text().replace('Grupa ', '');

        $(this).addClass('table-group-' + grp);

        $(this).find('tr:first th').each(function () {
            columns.push($(this).text().replace(/\s/g, '-').toLowerCase());
        });

        for (let i = 0; i < columns.length; ++i) {
            $(this).find('td:nth-child(' + (i + 1) + ')').addClass('column-' + columns[i]);
        }
    });

    $('.column-' + disciplineColumn).each(function () {
        $(this).attr('data-original', $(this).text());
    });

    wrapper.find("tr:contains('Curs')").addClass("curs-tr");
    wrapper.find("tr:contains('Seminar')").addClass("seminar-tr");
    wrapper.find("tr:contains('Laborator')").addClass("labor-tr");

    wrapper.find('tr:has(td)').each(setRowId);

    defaultHTML = wrapper.html();
}

function setRowId() {
    let row = $(this);

    columns = row.getColumns();

    let day = columns.day.text();
    let hour = columns.hour.text();
    let freq = columns.frequency.text();
    let grp = columns.group.text();
    let disc = columns.discipline.attr('data-original');

    let id = '';
    let dayi = -1;

    switch (day.substr(0, 2)) {
        case 'Lu':
            dayi = 0;
            break;
        case 'Ma':
            dayi = 1;
            break;
        case 'Mi':
            dayi = 2;
            break;
        case 'Jo':
            dayi = 3;
            break;
        case 'Vi':
            dayi = 4;
            break;
    }

    id += dayi;

    let hr = hour.split('-')[0];
    if (hr.length < 2) {
        hr = '0' + hr;
    }
    id += hr;
    if (/sapt\.\s[0-9]/.test(freq)) {
        id += 's' + freq.replace('sapt. ', '');
    }
    else {
        id += 's0';
    }
    id += grp;
    id += disc.replace(/\s/g, '-');

    idList.add(id);
    row.attr("data-id", id);
}

/**
    * @returns {day, hour, frequency, room, group,
    * 
    *  type, discipline, teacher}
    */

function checkSwitch() {
    let sel = $('#custom-selected-table').find('.ui-selected');
    let rem = $('#custom-list-table').find('.ui-selected');

    let selColumns = sel.getColumns();
    let remColumns = rem.getColumns();

    if (sel.length === 1) {
        highlightMatching();
    }
    else {
        clearHighlights();
    }

    if (sel.length === 1 && rem.length === 1 &&
        selColumns.discipline.text() === remColumns.discipline.text() &&
        selColumns.type.text() === remColumns.type.text()) {
        $('#custom-switch').button('enable');
    }
    else {
        $('#custom-switch').button('disable');
    }
}

function highlightMatching() {
    let sel = $('#custom-selected-table')
        .find('.ui-selected').eq(0).getColumns();

    $('#custom-list-table').addClass('matched-table');

    $('#custom-list-table tr').each(function () {
        let cols = $(this).getColumns();
        if (sel.discipline.text() === cols.discipline.text() &&
            sel.type.text() === cols.type.text()) {
            $(this).addClass('matching');
        }
    });
}

function clearHighlights() {
    $('#custom-list-table').find('.matching').removeClass('matching');
    $('#custom-list-table').removeClass('matched-table');
}

function listifiy(arr) {
    html = '<ul>';
    for (let i = 0; i < arr.length; ++i) {
        html += '<li>' + arr[i] + '</li>';
    }
    return html + '</ul>';
}

function getDisciplinesList() {
    let discSet = new Set();
    $('.column-' + disciplineColumn).each(function () {
        discSet.add($(this).text());
    });

    disciplines = [...discSet];
}

function rememberScroll() {
    customListScroll = $('#custom-list-wrapper .scroller').scrollTop();
}

function findDiscipline() {
    let disc = $('#finder-select').val();
    let type = $('#finder-type').val();

    let result = $('#finder-result');
    result.html('');

    $('#tables-wrapper tr').each(function () {
        if ($(this).find('.column-tipul').text().toLowerCase() === type &&
            ($(this).find('.column-disciplina').text() === disc ||
                $(this).find('.column-disciplina').attr('data-original') === disc)) {

            cloneStripped($(this)).appendTo(result);
        }
    });

    let rows = $('#finder-result tr').get();
    rows.sort(function (a, b) {
        a = $(a);
        b = $(b);

        let aDay = a.find('.column-ziua').text();
        let bDay = b.find('.column-ziua').text();
        if (aDay === bDay) {
            let aHr = a.find('.column-orele').text().split('-')[0];
            aHr = parseInt(aHr, 10);
            let bHr = b.find('.column-orele').text().split('-')[0];
            bHr = parseInt(bHr, 10);

            return aHr - bHr;
        }
        else {
            aDay = days.indexOf(aDay);
            bDay = days.indexOf(bDay);
            return aDay - bDay;
        }
    });


    rows = rows.filter(function (value, index, self) {
        if (index + 1 < self.length) {
            let tr = $(value);
            let nextTr = $(self[index + 1]);
            return !equalTrs(tr, nextTr);
        }
        return true;
    });
    $('#finder-result').html(rows);

    addDayLines($('#finder-result'));
}

function equalTrs(a, b) {
    let aDay = a.find('.column-ziua').text();
    let aHr = a.find('.column-orele').text();
    let aFreq = a.find('.column-frecventa').text();
    let aSala = a.find('.column-sala').text();
    let aForm = a.find('.column-formatia').text();
    let aType = a.find('.column-tipul').text();
    let aDisc = a.find('.column-disciplina').text();
    let aTeacher = a.find('.column-cadrul-didactic').text();

    let bDay = b.find('.column-ziua').text();
    let bHr = b.find('.column-orele').text();
    let bFreq = b.find('.column-frecventa').text();
    let bSala = b.find('.column-sala').text();
    let bForm = b.find('.column-formatia').text();
    let bType = b.find('.column-tipul').text();
    let bDisc = b.find('.column-disciplina').text();
    let bTeacher = b.find('.column-cadrul-didactic').text();

    return aDay === bDay && aHr === bHr && aFreq === bFreq && aSala === bSala &&
        aForm === bForm && aType === bType && aDisc === bDisc && aTeacher === bTeacher;
}

function updateFinderHourSelect() {
    let hours = new Set();
    $('tr:contains(' + $('#finder-day').val() + ')').each(function () {
        hours.add($(this).find('.column-orele').text());
    });
    hours = [...hours].sort(function (a, b) {
        a = parseInt(a.split('-')[0], 10);
        b = parseInt(b.split('-')[0], 10);
        return a - b;
    });

    $('#finder-hour').html('');
    for (let i = 0; i < hours.length; ++i) {
        $('#finder-hour').append('<option value="' + hours[i] + '">' + hours[i] + '</option>');
    }
    $('#finder-hour').selectmenu('refresh');

    findDisciplineByTime();
}

function findDisciplineByTime() {
    let day = $('#finder-day').val();
    let hour = $('#finder-hour').val();
    let week = $('#finder-sapt').val();

    let result = $('#finder-time-result');
    result.html('');

    $('#tables-wrapper tr').each(function () {
        let weekString = $(this).find('.column-frecventa').text();
        if ($(this).find('.column-ziua').text().toLowerCase() === day.toLowerCase() &&
            $(this).find('.column-orele').text() === hour &&
            (week == "0" || weekString.replace(/\s/g, '') === '' || weekString === 'sapt. ' + week)) {
            $(this)
                .clone()
                .removeClass('meh-tr')
                .removeClass('other-week-tr')
                .removeClass('nope-tr')
                .removeClass('last-of-day-tr')
                .appendTo(result);
        }
    });

    let rows = $('#finder-time-result tr').get();
    rows.sort(function (a, b) {
        a = $(a);
        b = $(b);

        let aGroup = a.find('.column-formatia').text();
        let bGroup = b.find('.column-formatia').text();
        if (aGroup === bGroup) {
            let aDisc = a.find('.column-disciplina').text();
            let bDisc = b.find('.column-disciplina').text();

            return compareStrings(aDisc, bDisc);
        }
        else {
            return compareStrings(aGroup, bGroup);
        }
    });
    rows = rows.filter(function (value, index, self) {
        if (index + 1 < self.length) {
            let tr = $(value);
            let nextTr = $(self[index + 1]);
            return !equalTrs(tr, nextTr);
        }
        return true;
    });
    $('#finder-time-result').html(rows);
}

function compareStrings(a, b) {
    if (a < b) {
        return -1;
    }
    else if (a > b) {
        return 1;
    }
    return 0;
}

function performChange() {
    saveOptions();
    updateSettingsUi();
    runFixer();
}

function updateSettingsUi() {
    let week = getWeekNo();
    if (week === 0) {
        week = 2;
    }
    $('#week-span').text("Week " + week + ": ");

    $('#offset-week')
        .prop('checked', options.weekOffset === 1)
        .checkboxradio('refresh');

    if ($('#tables-wrapper .column-frecventa:contains(sapt)').length === 0) {
        $('#week-wrapper').hide();
    }
    else {
        $('#week-wrapper').show();
    }



    if ($('#group-select:contains(' + options.group + ')').length) {
        $('#group-select')
            .val(options.group)
            .selectmenu('refresh');
    }

    if ($('#tables-wrapper table').length === 1) {
        $('#group-wrapper').hide();
    }
    else {
        $('#group-wrapper').show();
    }

    $('#half-group-select')
        .val(options.halfGroup)
        .selectmenu('refresh');

    $('#hidden-fade-select')
        .val(options.fadeHidden ? 'true' : 'false')
        .selectmenu('refresh');

    $('#other-halfgr-fade-select')
        .val(options.fadeHalfGroup ? 'true' : 'false')
        .selectmenu('refresh');

    $('#other-week-fade-select')
        .val(options.fadeWeek ? 'true' : 'false')
        .selectmenu('refresh');

    if ($('#tables-wrapper .column-formatia:contains(/1)').length === 0 &&
        $('#tables-wrapper .column-formatia:contains(/2)').length === 0) {
        $('#half-group-wrapper').hide();
    }
    else {
        $('#half-group-wrapper').show();
    }

    let hiderList = '<table>';
    let renamerList = '<table>';
    let finderOptions = '';

    let lDisciplines = disciplines.map(discipline => {
        let discName = discipline;
        if (options.disciplinesToReplace.hasOwnProperty(discipline) &&
            options.disciplinesToReplace[discipline]) {

            discName = options.disciplinesToReplace[discipline];
        }

        return {
            discipline,
            discName,
            hidden: isHidden(discipline)
        }
    });

    hiderList += lDisciplines.map(d =>
        `<tr>
            <td class="discipline-name" data-discipline="${d.discipline}">${d.discName}</td>
            <td class="check-column">
                <input class="hide-check" type="checkbox" ${(d.hidden ? 'checked' : '')}/>
            </td>
        </tr>`
    ).join('');

    sortedDisciplines = lDisciplines.sort((a, b) => {
        if (a.hidden == b.hidden) {
            a.discipline.localeCompare(b.discipline);
        } else {
            if (a.hidden) {
                return 1;
            }
            return -1;
        }
    });

    renamerList += sortedDisciplines.map(d => `
        <tr>
            <td class="discipline-name" >${d.discipline}</td>
            <td class="input-column">
                <input class="rename-to" type="text" value="${d.discipline == d.discName ? '' : d.discName}"/>
            </td>
        </tr>
    `).join('');

    finderOptions += sortedDisciplines.map(d => `
        <option value="${d.discipline}">${d.discName}</option>
    `).join('');


    hiderList += '</table>';
    renamerList += '</table>';

    $('#hider-list-wrapper').html(hiderList);
    $('#renamer-list-wrapper').html(renamerList);
    $('#finder-select').html(finderOptions);
    $('#finder-select').selectmenu('refresh');

    if (options.customTable.length == 0) {
        $('#custom-empty-view').show();
        $('#custom-work-view').hide();
    } else {
        $('#custom-empty-view').hide();
        $('#custom-work-view').show();
        updateCustomWorkView();
    }
}

function updateCustomWorkView() {
    let chosen = $('#custom-selected-table');
    chosen.html('');

    let remList = $('#custom-list-table');
    remList.html('');


    let remaining = new Set(idList);

    for (let i = 0; i < options.customTable.length; ++i) {
        let id = options.customTable[i];

        remaining.delete(id);

        let row = cloneStripped($('tr[data-id="' + id + '"]').eq(0));
        shortenInfo(row);
        chosen.append(row);
    }
    chosen.selectable('refresh');
    addDayLinesCustom(chosen);

    remaining = Array.from(remaining);
    sortIds(remaining);

    for (let i = 0; i < remaining.length; ++i) {
        let id = remaining[i];

        let row = cloneStripped($('tr[data-id="' + id + '"]').eq(0));
        shortenInfo(row);
        remList.append(row);
    }
    remList.selectable('refresh');
    addDayLinesCustom(remList);

    $('#custom-add, #custom-remove, #custom-switch').button('disable');

    $('#custom-list-wrapper .scroller').scrollTop(customListScroll);
}

function sortIds(array) {
    array.sort();
}

function cloneStripped(element) {
    return element
        .clone()
        .removeClass('meh-tr')
        .removeClass('hide-tr')
        .removeClass('other-week-tr')
        .removeClass('nope-tr')
        .removeClass('last-of-day-tr');
}

function shortenInfo(row) {
    if (row.length == 0) {
        return;
    }
    let columns = row.getColumns();
    /*
     * @returns {day, 
     * hour, 
     * frequency, room, group, type, discipline, teacher}
     */

    let day = columns.day.text();
    columns.day.text(day.substr(0, 2)).attr('title', day);

    let hour = columns.hour.text();
    columns.hour.text(hour.split('-')[0]).attr('title', hour);

    let sapt = columns.frequency.text();
    columns.frequency.text(sapt.replace('sapt. ', 's')).attr('title', sapt);

    let room = columns.room.text();
    columns.room.text(room.substr(0, 4) + ((room.length > 4) ? '.' : '')).attr('title', room);

    let type = columns.type.text();
    columns.type.text(type.substr(0, 1)).attr('title', type);

    let disc = columns.discipline.text();
    columns.discipline.attr('title', disc);

    let teacher = columns.teacher.text();
    let teach = teacher.split(' ');
    let tStr = teach[1];

    let tt = tStr.split('-');
    tStr = tt[0].substr(0, 1) + tt[0].substr(1).toLowerCase();

    for (let i = 1; i < tt.length; ++i) {
        tStr += '-' + tt[i].substr(0, 1) + '.';
    }

    for (let i = 2; i < teach.length; ++i) {
        tStr += ' ' + teach[i].substr(0, 1) + '.';
    }
    columns.teacher.text(tStr).attr('title', teacher);
}

function runFixer() {
    'use strict';

    $('#tables-wrapper').html(defaultHTML);

    let myGroupHeading = $("h1:contains('" + options.group + "')");
    if ($('#tables-wrapper table').length === 1) {
        myGroupHeading = $('#tables-wrapper h1');
    }
    let myGroupTable = myGroupHeading.next("table");

    myGroupHeading.prependTo("#tables-wrapper").css("margin", "30px");
    myGroupTable.insertAfter(myGroupHeading).css("margin-bottom", "147px");

    let hasCustom = options.customTable.length > 0;

    if (hasCustom) {
        if (!insertCustomTable()) {
            options.customTable = [];
            customError = true;
            performChange();
            return;
        }
        else {
            customError = false;
        }
    }
    if (customError) {
        $('#tables-wrapper').prepend('<hgroup><h1 class="error">Some custom items were not found</h1><h2 class="error">The timetable might have changed. The custom table has been cleared.</h2></hgroup>');
    }
    let customTable = $('.table-custom');

    for (let discipline in options.disciplinesToReplace) {
        if (options.disciplinesToReplace.hasOwnProperty(discipline) && options.disciplinesToReplace[discipline]) {
            let a = $('.column-' + disciplineColumn + ' a:contains(' + discipline + ')');
            a.each(function () {
                let text = $(this).text();
                $(this)
                    .text(text.replace(discipline, options.disciplinesToReplace[discipline]));
            });
        }
    }

    fixTable(myGroupTable);
    if (hasCustom) {
        fixTable(customTable, true);
    }


    $("table").each(function () {
        addDayLines($(this));
    });
}

function fixTable(table, custom = false) {

    for (let i = 0; i < options.disciplinesToHide.length; ++i) {
        let discipline = options.disciplinesToHide[i];
        let row = table
            .find('.column-' + disciplineColumn + '[data-original="' + discipline + '"]')
            .closest('tr');

        if (options.fadeHidden) {
            row.addClass("meh-tr")
                .addClass("nope-tr");
        }
        else {
            row.addClass("hide-tr");
        }
    }

    if (!custom) {
        let otherHgTrs = table.find("tr:contains('" + options.group + "/" + (3 - options.halfGroup) + "')");

        if (options.fadeHalfGroup) {
            otherHgTrs.addClass("meh-tr");
        }
        else {
            otherHgTrs.addClass("hide-tr");
        }
    }

    let weekNo = getWeekNo();
    let otherWeekNo = weekNo + 1;

    let saptString = "sapt. " + otherWeekNo;

    let otherWeekTrs = table.find("tr:contains(" + saptString + ")");

    if (options.fadeWeek) {
        otherWeekTrs.addClass("other-week-tr");
    }
    else {
        otherWeekTrs.addClass("hide-tr");
    }
}

function insertCustomTable() {
    let table = $('<table class="table-custom">');
    table.append($('#tables-wrapper table').eq(0).find('tr:has(th)').clone());

    for (let i = 0; i < options.customTable.length; ++i) {
        let id = options.customTable[i];

        let row = $('tr[data-id="' + id + '"]');
        if (row.length > 0) {
            row = row.eq(0);
            table.append(row.clone());
        }
        else {
            console.error(id + ' NOT FOUND');
            return false;
        }
    }

    let customHeading = $('<h1>Custom</h1>');

    customHeading.prependTo("#tables-wrapper").css("margin", "30px");
    table.insertAfter(customHeading);

    return true;
}

function addDayLines(table) {
    let lastVisible = table.find('tr:visible:last');

    for (let i in days) {
        let dayString = days[i];
        let last = table.find("tr:visible:contains(" + dayString + ")");

        last = $(last[last.length - 1]);
        if (!last.is(lastVisible)) {
            last.addClass("last-of-day-tr");
        }
    }
}

function addDayLinesCustom(table) {
    for (let i in days) {
        let dayString = days[i];
        let last = table.find("td[title='" + dayString + "']").closest('tr');

        last = $(last[last.length - 1]);
        if (!last.is(':last-child')) {
            last.addClass("last-of-day-tr");
        }
    }
}

function getWeekNo() {
    let weekInYear = new Date().getWeek();
    let firstWeek = new Date(new Date().getYear(), 0, 1).getWeek();

    return (weekInYear - firstWeek + 1 + options.weekOffset) % 2;
}
