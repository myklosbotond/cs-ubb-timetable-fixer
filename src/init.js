function setupFunctions() {
    'use strict';

    Date.prototype.getWeek = function () {
        var onejan = new Date(this.getFullYear(), 0, 1);
        return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
    };

    /**
     * @returns {day, hour, frequency, room, group, type, discipline, teacher}
     */
    jQuery.fn.getColumns = function () {
        if (this.is('tr')) {
            return {
                day: this.find('.column-ziua'),
                hour: this.find('.column-orele'),
                frequency: this.find('.column-frecventa'),
                room: this.find('.column-sala'),
                group: this.find('.column-formatia'),
                type: this.find('.column-tipul'),
                discipline: this.find('.column-disciplina'),
                teacher: this.find('.column-cadrul-didactic')
            };
        }
        else {
            return {};
        }
    };

    setDialogsPositionFixed();
    setFavicon();
}

function setDialogsPositionFixed() {
    $.ui.dialog.prototype._oldinit = $.ui.dialog.prototype._init;
    $.ui.dialog.prototype._init = function () {

        $(this.element).parent().css("position", "fixed");
        $(this.element).parent().addClass("no-select");
        $(this.element).parent().css("margin", "5px"); //limit 5px to the top
        $(this.element).dialog("option", {
            resizeStart: function (event, ui) {
                $(event.target).parent().css("position", "fixed");
                return true;
            },
            resizeStop: function (event, ui) {
                $(event.target).parent().css("position", "fixed");
                return true;
            }
        });
        this._oldinit();
    };
}

function setFavicon() {
    const href = 'https://i.imgur.com/s6FbuBK.png';
    $('head').append(`<link rel="shortcut icon" type="image/png" href="${href}"/>`);
}
