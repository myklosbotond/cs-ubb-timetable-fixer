function injectSettingHTML() {
    'use strict';

    let groupOptions = '';
    $('#tables-wrapper h1').each(function () {
        try {
            let group = parseInt($(this).text().replace('Grupa ', ''), 10);
            let sel = '';
            if (group == options.group) {
                sel = 'selected';
            }
            groupOptions += `<option value="${group}" ${sel}>${group}</option>`;
        }
        catch (e) {
            console.log(e);
        }
    });

    const halfGroupOptions = [[1, 1], [2, 2], [0, "Don't hide"]].map(([val, txt]) =>
        `<option value="${val}" ${val === options.halfGroup ? "selected" : ""}>${txt}</option>`
    );

    let html =
        `<div id="settings-container">
            <button id="toggle-settings">Settings</button>
            <div id="settings-wrapper">
            <div id="week-wrapper" class="option-wrapper">
                <span id="week-span"></span>
                <label id="offset-label" for="offset-week">Offset week by 1</label>
                <input type="checkbox" id="offset-week" />
            </div>
            <div id="group-wrapper" class="option-wrapper">
                <label for="group-select">Group: </label>
                <select id="group-select">
                    <option value="0"></option>
                    ${groupOptions}
                </select>
            </div>
            <div id="half-group-wrapper" class="option-wrapper">
                <label for="half-group-select">Halfgroup: </label>
                <select id="half-group-select">
                    ${halfGroupOptions}
                </select>
            </div>
            <div class="option-wrapper">
                <button id="visibility-button">Edit Visibility</button>
            </div>
            <br>
            <div id="buttons-wrapper" class="option-wrapper">
                <button id="hider-button">Hidden disciplines</button>
                <button id="renamer-button">Rename disciplines</button>
                <button id="finder-button">Find disciplines</button>
                <button id="reset-button">Reset Settings</button>
                <button id="print-button">Print this page</button>
            </div>
            <br>
            <div id="buttons-wrapper-2" class="option-wrapper">
                <button id="import-button">Import settings</button>
                <button id="export-button">Export settings</button>
                <a id="download-helper" class="ui-helper-hidden"></a>
                <button id="customize-button" class="beta-button">Edit custom</button>
            </div><br>`;
    if (DEVELOPEMENT) {
        html += '<div id="urgent-todo" class="todo-wrapper">' + listifiy(URGENT_TODO) + '</div>';
        html += '<div id="todo" class="todo-wrapper">' + listifiy(TODO) + '</div>';
    }

    html += `
        <div id="visibility-dialog">
            <div id="hidden-fade-wrapper" class="row no-gutters">
                <label for="hidden-fade-select" class="col-7">Hidden items: </label>
                <div class="col-5">
                    <select id="hidden-fade-select">
                        <option value="true">Faded</option>
                        <option value="false">Hidden</option>
                    </select>
                </div>
            </div>
            <div id="other-halfgr-fade-wrapper" class="row no-gutters">
                <label for="other-halfgr-fade-select" class="col-7">Other halfgroup's items: </label>
                <div class="col-5">
                    <select id="other-halfgr-fade-select">
                        <option value="true">Faded</option>
                        <option value="false">Hidden</option>
                    </select>   
                </div>
            </div>
            <div class="row no-gutters">
                <label for="other-week-fade-select" class="col-7">Other week items: </label>
                <div class="col-5">
                    <select id="other-week-fade-select">
                        <option value="true">Faded</option>
                        <option value="false">Hidden</option>
                    </select>   
                </div>
            </div>
        </div>`;

    html += `
        <div id="hider-dialog">
            <span>Select the disciplines you <b>don\'t</b> want highlighted:</span>
            <div id="hider-list-wrapper"></div>
            <div style="float: right;">
                <button id="hider-save">Save</button>
                <button id="hider-close">Cancel</button>
            </div>
        </div>`;

    html += `
        <div id="renamer-dialog">
            <span>Enter the text you want each discipline to be renamed to. <br> Leave the field blank if you don\'t want a discipline\'s name replaced.</span>
            <div id="renamer-list-wrapper"></div>
            <div style="float: right;">
                <button id="renamer-save">Save</button>
                <button id="renamer-close">Cancel</button>
            </div>
        </div>`;

    html += `
        <div id="finder-dialog">
            <div id="tabs">
                <ul>
                    <li><a href="#tabs-1">By Discipline</a></li>
                    <li><a href="#tabs-2">By Time</a></li>
                </ul>
                <div id="tabs-1">
                    <div class="finder-options-wrapper">
                        <label for="finder-select">Discipline to search for: </label>
                        <select id="finder-select"></select>
                        
                        <label for="finder-type"> Type: </label>
                        <select id="finder-type">
                            <option value="curs">Course</option>
                            <option value="seminar">Seminar</option>
                            <option value="laborator">Laboratory</option>
                        </select>
                    </div>
                    <table id="finder-result"></table>
                </div>
                <div id="tabs-2">
                    <div class="finder-options-wrapper">
                        <label for="finder-day">Day: </label><select id="finder-day"></select>
                        <label for="finder-hour"> Hour: </label>
                        <select id="finder-hour"></select>
                        <label for="finder-sapt"> Week: </label>
                        <select id="finder-sapt">
                            <option value="0">Both</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                    </div>
                    <table id="finder-time-result"></table>
                </div>
            </div>
        </div>`;

    html += `
        <div id="import-dialog">
            <span id="import-status"></span>
            <label for="import-file">Choose settings file:</label>
            <input type="file" id="import-file" accept=".json,text/json"/>
            <div style="float: right; margin-top: 20px;">
                <button id="do-import">Import</button>
            </div>
        </div>`;

    html += `
        <div id="customize-dialog">
            <div id="custom-empty-view">
                <label for="custom-group-select">List empty. Start with group: </label>
                <select id="custom-group-select">
                    ${groupOptions}
                </select>
                <label for="custom-half-group-select">Halfgroup: </label>
                <select id="custom-half-group-select">
                    ${halfGroupOptions}
                </select>
                <hr>
                <button id="custom-start">Go</button>
            </div>
            <div id="custom-work-view">
                <div id="custom-selected-wrapper">
                    <h3>Your items</h3>
                    <div class="scroller">
                        <table id="custom-selected-table"></table>
                    </div>
                </div>
                <div id="custom-controls">
                    <button id="custom-add" title="Add">&lt;&lt;</button>
                    <hr>
                    <button id="custom-remove" title="Remove">&gt;&gt;</button>
                    <hr>
                    <button id="custom-switch" title="Switch">&lt;&gt;</button>
                </div>
                <div id="custom-list-wrapper">
                    <h3>Other items</h3>
                    <div class="scroller">
                        <table id="custom-list-table"></table>
                    </div>
                </div>
                <hr>
                <button id="custom-clear" style="margin-left: 42%;">Clear All</button>
            </div>
        </div>`;
    html += '</div></div>';
    $(html).insertAfter('#title-heading');

    if (options.settingsHidden) {
        $('#settings-wrapper').hide();
    }

    for (let i = 0; i < days.length; ++i) {
        $('#finder-day').append('<option value="' + days[i] + '">' + days[i] + '</option>');
    }

    $('#offset-week').checkboxradio({
        icon: false
    });
    $('button').button();
    $('select').selectmenu();

    $('#toggle-settings').button({
        icon: 'ui-icon-gear',
        text: false
    });
    $('#visibility-button').button({
        icon: 'ui-icon-eye'
    });
    $('#hider-button').button({
        icon: 'ui-icon-hide'
    });
    $('#renamer-button').button({
        icon: 'ui-icon-rename'
    });
    $('#finder-button').button({
        icon: 'ui-icon-find'
    });
    $('#reset-button').button({
        icon: 'ui-icon-reset'
    });
    $('#print-button').button({
        icon: 'ui-icon-printer'
    });
    $('#import-button').button({
        icon: 'ui-icon-import'
    });
    $('#export-button').button({
        icon: 'ui-icon-export'
    });
    $('#customize-button').button({
        icon: 'ui-icon-customize'
    });

    $('#tabs').tabs();

    $('#visibility-dialog').dialog({
        modal: true,
        width: 320,
        title: 'Edit visibility options',
        autoOpen: false
    });

    $('#hider-dialog').dialog({
        modal: true,
        width: 320,
        title: 'Hide disciplines',
        autoOpen: false
    });

    $('#renamer-dialog').dialog({
        modal: true,
        width: 420,
        title: 'Rename disciplines',
        autoOpen: false
    });

    $('#finder-dialog').dialog({
        modal: true,
        width: 620,
        height: 'auto',
        title: 'Find disciplines',
        autoOpen: false
    });
    updateFinderHourSelect();

    $('#import-dialog').dialog({
        modal: true,
        width: 320,
        height: 'auto',
        title: 'Import settings',
        autoOpen: false
    });
    $('#do-import').button("option", "disabled", true);

    $('#customize-dialog').dialog({
        modal: true,
        minWidth: 820,
        height: 'auto',
        title: 'Edit your custom timetable',
        autoOpen: false
    });

    $('#custom-selected-table').selectable({
        filter: 'tr',
        selected: function () {
            $('#custom-remove').button('enable');

            checkSwitch();
        },
        unselected: function () {
            if ($(this).find('.ui-selected').length == 0) {
                $('#custom-remove').button('disable');
            }
            checkSwitch();
        }
    });
    $('#custom-list-table').selectable({
        filter: 'tr',
        selected: function () {
            $('#custom-add').button('enable');

            checkSwitch();
        },
        unselected: function () {
            if ($(this).find('.ui-selected').length == 0) {
                $('#custom-add').button('disable');
            }
            checkSwitch();
        }
    });
}


function setEventHandlers() {
    $('body')
        .on('click', '#toggle-settings', function () {
            $('#settings-wrapper').slideToggle();
            options.settingsHidden = !options.settingsHidden;
            saveOptions();
        })
        .on('change', '#offset-week', function () {
            options.weekOffset = ($(this).prop('checked') ? 1 : 0);
            performChange();
        })
        .on('selectmenuchange', '#group-select', function () {
            options.group = parseInt($(this).val(), 10);
            performChange();
        })
        .on('selectmenuchange', '#half-group-select', function () {
            options.halfGroup = parseInt($(this).val(), 10);
            performChange();
        })
        .on('selectmenuchange', '#hidden-fade-select', function () {
            let value = $(this).val() === 'true';
            options.fadeHidden = value;
            performChange();
        })
        .on('selectmenuchange', '#other-halfgr-fade-select', function () {
            let value = $(this).val() === 'true';
            options.fadeHalfGroup = value;
            performChange();
        })
        .on('selectmenuchange', '#other-week-fade-select', function () {
            let value = $(this).val() === 'true';
            options.fadeWeek = value;
            performChange();
        })
        .on('click', '#visibility-button', function () {
            $('#visibility-dialog')
                .dialog('option', 'position', {
                    my: 'center top',
                    at: 'center top+50',
                    of: window
                })
                .dialog('open');
        })
        .on('click', '#hider-button', function () {
            $('#hider-dialog')
                .dialog('option', 'position', {
                    my: 'center top',
                    at: 'center top+50',
                    of: window
                })
                .dialog('open');
        })
        .on('click', '#renamer-button', function () {
            $('#renamer-dialog')
                .dialog('option', 'position', {
                    my: 'center top',
                    at: 'center top+50',
                    of: window
                })
                .dialog('open');
        })
        .on('click', '#reset-button', function () {
            if (confirm('Are you sure you want to reset?')) {
                options = getDefaultOptions();
                performChange();
            }
        })
        .on('click', '#import-button', function () {
            $('#import-dialog')
                .dialog('option', 'position', {
                    my: 'center top',
                    at: 'center top+50',
                    of: window
                })
                .dialog('open');
        })
        .on('click', '#export-button', exportOptions)
        .on('click', '#print-button', window.print)
        .on('click', '#hider-save', function () {
            let hides = [];
            $('#hider-list-wrapper tr').each(function () {
                if ($(this).find('.hide-check').is(':checked')) {
                    hides.push($(this).find('.discipline-name').attr('data-discipline'));
                }
            });

            $('#hider-dialog').dialog('close');
            options.disciplinesToHide = hides;
            performChange();
        })
        .on('click', '#hider-close', function () {
            $('#hider-dialog').dialog('close');
        })
        .on('click', '#renamer-save', function () {
            let renames = {};
            $('#renamer-list-wrapper tr').each(function () {
                let renameTo = $(this).find('.rename-to').val();
                if (renameTo.length > 0) {
                    renames[$(this).find('.discipline-name').text()] = renameTo;
                }
            });

            options.disciplinesToReplace = renames;

            $('#renamer-dialog').dialog('close');
            performChange();
        })
        .on('click', '#renamer-close', function () {
            $('#renamer-dialog').dialog('close');
        })
        .on('click', '#finder-button', function () {
            $('#finder-dialog')
                .dialog('option', 'position', {
                    my: 'center top',
                    at: 'center top+50',
                    of: window
                })
                .dialog('open');
            findDiscipline();
        })
        .on('selectmenuchange', '#finder-dialog select', findDiscipline)
        .on('selectmenuchange', '#finder-day', updateFinderHourSelect)
        .on('selectmenuchange', '#finder-hour, #finder-sapt', findDisciplineByTime)

        .on('change', '#import-file', function () {
            let files = $(this)[0].files;
            $('#do-import').button("option", "disabled", files.length < 1);
        })
        .on('click', '#do-import', importOptions)
        .on('click', '#customize-button', function () {
            $('#customize-dialog')
                .dialog('option', 'position', {
                    my: 'center top',
                    at: 'center top+20',
                    of: window
                })
                .dialog('open');
        })
        .on('click', '#custom-start', function () {
            initCustomTable();
            performChange();
        })
        .on('click', '#custom-clear', function () {
            if (confirm("Are you sure you want to clear everything?")) {
                options.customTable = [];
                performChange();
            }
        })
        .on('click', '#custom-add', function () {
            $('#custom-list-table .ui-selected').each(function () {
                options.customTable.push($(this).attr('data-id'));
            });
            sortIds(options.customTable);
            rememberScroll();
            performChange();
        })
        .on('click', '#custom-remove', function () {
            $('#custom-selected-table .ui-selected').each(function () {
                removeElement(options.customTable, $(this).attr('data-id'));
            });
            rememberScroll();
            performChange();
        })
        .on('click', '#custom-switch', function () {
            $('#custom-list-table .ui-selected').each(function () {
                options.customTable.push($(this).attr('data-id'));
            });
            $('#custom-selected-table .ui-selected').each(function () {
                removeElement(options.customTable, $(this).attr('data-id'));
            });
            sortIds(options.customTable);
            rememberScroll();
            performChange();
            clearHighlights();
        });
}
