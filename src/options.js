function getDefaultOptions() {
    'use strict';

    return {
        weekOffset: 0,
        group: 0,
        halfGroup: 0,
        disciplinesToReplace: {},
        disciplinesToHide: [],
        customTable: [],
        fadeHidden: true,
        fadeHalfGroup: true,
        fadeWeek: true,
        settingsHidden: false
    };
}

function getBaseOptionKey() {
    return "options";
}

function getOptionKey() {
    parts = location.pathname.match('/([0-9]+-[0-9]+)/.*/([^.]+)');
    try {
        return getBaseOptionKey() + "_" + parts[1] + "_" + parts[2];
    }
    catch (err) {
        return getBaseOptionKey();
    }
}

function loadOptions() {
    'use strict';

    let defOptions = getDefaultOptions();
    let savedOptions = GM_getValue(getOptionKey(), GM_getValue(getBaseOptionKey(), "{}"));
    try {
        savedOptions = JSON.parse(savedOptions);
    }
    catch (err) {
        savedOptions = {};
    }

    options = $.extend(defOptions, savedOptions);
}


function exportOptions() {
    let dataStr = 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(options));
    let downloadHelper = $('#download-helper')

    downloadHelper.attr('href', dataStr);
    downloadHelper.attr('download', 'fixer-options.json');
    downloadHelper[0].click();
}

function importOptions() {
    let files = $('#import-file')[0].files;
    if (files.length > 0) {
        let file = files[0];
        let reader = new FileReader();

        reader.onload = function (e) {
            parseOptionFile(reader.result);
        }

        reader.readAsText(file);
    }
}

function parseOptionFile(text) {
    try {
        options = JSON.parse(text);
        performChange();
        $('#import-dialog').dialog('close');
        $('#import-status').text("");
    }
    catch (err) {
        $('#import-status').text("Import failed. Invalid JSON.");
        console.error(err);
    }
}

function saveOptions() {
    GM_setValue(getOptionKey(), JSON.stringify(options));
}


function isHidden(disc) {
    return options.disciplinesToHide.includes(disc);
}
