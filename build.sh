#!/bin/bash
#   A bash script that generates the output .js file.
#   I needed this, because the single file was getting out of hand.

# ==== Utilities ====
RED='\033[0;31m'
NC='\033[0m' # No Color

echoerr() { echo -e "${RED}$@${NC}" 1>&2; }

showUsage() {
    echoerr "Usage: $0 <mode>"
    echoerr "  MODE:"
    echoerr "    dev - developer mode"
    echoerr "    prod - production mode"
}

# ==== CONSTANTS ====
distDir="./dist"
srcDir="./src"
fileName="OrarFixer"

fixedFiles="meta.js constants.js main.js css.js"

# ==== Usage check ====
if [ $# -lt 1 ]; then
    showUsage
    exit 1
else
    if [ $1 != "dev" -a $1 != "prod" ]; then
        showUsage
        exit 2
    fi
    mode=$1
fi

# ==== Script ====

#CheckFixedOk
fixedOk=0
for fixedFile in $fixedFiles; do 
    fixedPath="${srcDir}/${fixedFile}"
    if [ ! -f $fixedPath ]
    then echoerr "File ${fixedPath} must exist!"
        fixedOk=1
    fi
done
if [ $fixedOk -ne 0 ]; then 
    exit 9
fi

version=`cat ./src/meta.js | egrep '@version' | sed -r 's/^\/\/\s+@version\s+//'`

if [ $mode = "prod" ]; then
    sVersion=$version
else
    sVersion='DEV'
fi

#CreateOutput 
outFile="${distDir}/${fileName}.v${sVersion}.user.js"
rm $outFile 2>/dev/null
touch $outFile

#appendCode
files=`ls $srcDir | egrep '.*\.js$'`
for fixedFile in $fixedFiles
do 
    if [ $fixedFile = "constants.js" ]; then
        if [ $mode = "prod" ]; then
            echo "const DEVELOPEMENT = false;" >> $outFile
        else
            echo "const DEVELOPEMENT = true;" >> $outFile
            echo "GM_addStyle('body::after { content: \"v${version}\"; position: fixed; bottom: 5px; right: 5px; }');" >> $outFile
        fi
    fi
    if [ $fixedFile = "css.js" ]; then
        awk -v srcDir="$srcDir" '/<style.css>/ { 
            print "    GM_addStyle(`"
            system ("cat " srcDir "/style.css | sed \"s/^/        /\"") 
            print "    `);"
        } \
        !/<style.css>/ { print; }' "${srcDir}/css.js" >> $outFile
    else
        cat "${srcDir}/${fixedFile}" >> $outFile
    fi
    echo "" >> $outFile

    files=`echo $files | sed -r "s/\b${fixedFile}\b//"`
done

for file in $files; do
    cat "${srcDir}/${file}" >> $outFile    
    echo "" >> $outFile
done

